import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(appBar: AppBar(

        title: Text('AppVesto Day 1 App'),
          ),
        body: Center(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.purpleAccent,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.7),
                    blurRadius: 13.0, // softening the shadow
                    spreadRadius: 1.0, // extending the shadow
                    offset: Offset(
                      2.0, // horizontal
                      2.0, // vertical
                    ),
                  )
                ],
              ),
              height: 200,
              width: 200,
              child: Center(
                child: Text(
                  'Hello!',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ),
            ),
        ),
      ),
    );
  }
}